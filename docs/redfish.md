# Redfish cheatsheet

!!! note
    `$USERNAME` and `$PASSWORD` are the IPMI credentials.


## Query BIOS version
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/UpdateService/FirmwareInventory/BIOS | jq .
```

## Query BMC version
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/UpdateService/FirmwareInventory/BMC | jq .
```

## Docs about the different options in BIOS
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/Registries/BiosAttributeRegistry.json |jq . |less
```

## Example on how to update a BIOS setting (numa nodes)

### First we check the current value
```
$>  curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/Systems/1/Bios | jq . | egrep Rome0075|tail -1
    "Rome0075": "NPS1",

```

### Now we update the settings from NPS1 to NPS4 and reboot
```
$> curl -v -k -u $USERNAME:$PASSWORD --request PATCH  https://${BMC_IP}/redfish/v1/Systems/Self/Bios/SD  --header 'If-Match: *' --header 'If-None-Match: "123456"' --header 'Content-Type: application/json' --data-raw '{"Attributes": {"Rome0075": "NPS4"}}'
```

When updating the settings you should get a 204 return code (see below the curl output when using `curl -v`)
```
< HTTP/1.1 204 No Content
< Server: AMI MegaRAC Redfish Service
```

Now we reboot the node so the new BIOS settings are applied:
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X POST https://${BMC_IP}/redfish/v1/Chassis/Self/Actions/Chassis.Reset -d '{"ResetType": "ForceRestart"}'
```

### Check new settings have been applied

After the machine reboots double check that the new settings have been applied
```
$>  curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/Systems/1/Bios | jq . | egrep Rome0075|tail -1
    "Rome0075": "NPS4",
```

## Query CPU details
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/Systems/Self/Processors |jq .
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/Systems/Self/Processors/CPU0 |jq .
```

## Query the board serial number (this is the default ipmi password when upgrading to the latest BMC firmware in gigabyte systems)
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/Chassis/Self | jq . |grep -i board
```

## Force poweroff and start
```
curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X POST https://${BMC_IP}/redfish/v1/Chassis/Self/Actions/Chassis.Reset -d '{"ResetType": "ForceOff"}'
curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X POST https://${BMC_IP}/redfish/v1/Chassis/Self/Actions/Chassis.Reset -d '{"ResetType": "On"}'
```

## Force a reboot
```
curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X POST https://${BMC_IP}/redfish/v1/Chassis/Self/Actions/Chassis.Reset -d '{"ResetType": "ForceRestart"}'
```

## Restart BMC interface (ipmi)

### This one is to query the allowed arguments
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X GET https://${BMC_IP}/redfish/v1/Managers/Self/ResetActionInfo |jq .
```

###This one is to actually restart BMC
```
$> curl -s -k -u $USERNAME:$PASSWORD -H"Content-type: application/json" -X POST https://${BMC_IP}/redfish/v1/Managers/Self/Actions/Manager.Reset -d '{"ResetType": "ForceRestart"}'
```
